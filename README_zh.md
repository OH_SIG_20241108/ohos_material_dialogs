# material-dialogs

## 简介
> material-dialogs是自定义对话框库。

![operation.gif](screenshots/operation.gif)

## 下载安装
```shell
ohpm install @ohos/material-dialogs
```

## 使用说明
1. 引入文件及代码依赖
 ```
    import { MaterialDialog } from '@ohos/material_dialogs'
 ```
2. 初始化Model数据
 ```
   @State model: MaterialDialog.Model= new MaterialDialog.Model();

 ```
3. 初始化对话框控制器
 ```
    dialogController: CustomDialogController = new CustomDialogController({
    builder: MaterialDialog({
      model: this.model
    }),
    cancel: this.existDialog,
    autoCancel: true,
    alignment: DialogAlignment.Center
    })
 ```

4. 调用Model对应函数并展示目标对话框
 ```
    this.model.reset()
    this.model.message($r('app.string.useOhosLocationServicesPrompt'))
    this.model.positiveButton($r('app.string.agree'), {
     onClick() {
       console.info('ClickCallback when the confirm button is clicked')
     }
    })
    this.model.negativeButton($r('app.string.disagree'), {
     onClick() {
       console.info('ClickCallback when the cancel button is clicked')
     }
    })
    this.model.setScrollHeight(120)
    this.dialogController.open()
 ```

## 接口说明
`@State model: MaterialDialog.Model= new MaterialDialog.Model();`
1. 显示图标`model.icon()`
2. 显示标题`model.title()`
3. 显示描述信息`model.message()`
4. 显示确定按钮`model.positiveButton()`
5. 显示取消按钮`model.negativeButton()`
6. 显示中立按钮`model.neutralButton()`
7. 按钮是否堆叠显示`model.setStacked()`
8. 显示复选框`model.checkBoxPrompt()`
9. 设置按钮是否可用`model.setActionButtonEnabled()`
10. 显示列表内容`model.listItems()`
11. 显示单选列表内容`model.listItemsSingleChoice()`
12. 显示多选列表内容`model.listItemsMultiChoice()`
13. 显示输入框`model.input()`
14. 显示颜色选择器`model.colorChooser()`
15. 显示日期时间选择器`model.dateTimePicker()`

## 约束与限制
在下述版本验证通过：
- DevEco Studio: NEXT Beta1-5.0.3.806, SDK: API12 Release(5.0.0.66)

- DevEco Studio 4.1 Release(4.1.0.400), SDK: API11 (4.1.0.400)

- DevEco Studio: 4.0 Beta2(4.0.3.512), SDK: API10 (4.0.10.9)

- DevEco Studio: 4.0 Canary1(4.0.0.112), SDK: API10 (4.0.7.2)

## 目录结构
````
|---- material-dialogs
|     |---- entry  # 示例代码文件夹
|     |---- material_dialogs  # material_dialogs库文件夹
|           |---- index.ets  # 对外接口
|           |---- src
|                 |---- main
|                       |---- components
|                             |---- MaterialDialog.ets  # 自定义组件类
|                             |---- ClickCallback.ets  # 点击事件
|                             |---- InputCallback.ets  # 输入内容变化事件
|                             |---- ItemListener.ets  # 条目点击事件
|     |---- README.md  # 安装使用方法                    
|     |---- README_zh.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/material-dialogs/issues) 给组件，当然，也非常欢迎发 [PR](https://gitee.com/openharmony-sig/material-dialogs/pulls)共建 。

## 开源协议
本项目基于 [ Apache License 2.0](https://gitee.com/openharmony-sig/material-dialogs/blob/master/LICENSE) ，请自由地享受和参与开源。